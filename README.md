
# Wordle

Web services desarrolladas con NestJS y el uso de un ORM llamado Prisma.
Base de datos creada con PostgreSQL.


## Authors

- [@JMDHC](https://www.github.com/JMDHC)


## API Documentation

[Documentation](https://wordle-app-dd3.herokuapp.com/docs/)


## Features

- Tiene almacenado un diccionario de palabras
- Se selecciona del diccionario una palabra de 5 letras cada 5 minutos y no se deberá repetir la palabra.
- Permite comparar cada letra entre dos palabras (la que ingresa el usuario y la seleccionada). La palabra del usuario debe contener 5 letras. Se regresarán los siguientes estatus por letra:

        a. Si la letra ingresada está en el mismo lugar, regresará un 1 y sumará 1
        intento al usuario

        b. Si la letra ingresada está en la palabra pero no en el mismo lugar, regresará
        la letra con un 2 y sumará 1 intento al usuario

        c. Si la letra ingresada no se encuentra en la palabra, regresará la letra con un
        3 y sumará 1 intento al usuario

        d. Si acertó en todas las letras, deberá guardar que el usuario acertó la palabra

- Un usuario solo tiene 5 intentos para evaluar la palabra
- Cada 5 minutos se selecciona una nueva palabra, no se deberá repetir la palabra
- Cada vez que se genera una nueva palabra, el contador de intentos se reinicia a 0 para todos los usuarios
- Permite obtener cuantas partidas a jugado un usuario y cuantas victorias ha tenido
- Permite obtener los mejores 10 jugadores con su número de victorias
- Permite obtener las palabras más acertadas
- Los servicios de la aplicación estar protegidos
- Los servicios cuentan con test (Feature no incluido)