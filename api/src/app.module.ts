import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaModule } from './prisma/prisma.module';
import { SessionModule } from './session/session.module';
import { UsersModule } from './users/users.module';
import { GamesModule } from './games/games.module';
import { ScheduleModule } from '@nestjs/schedule';
import { WordsModule } from './words/words.module';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    PrismaModule,
    SessionModule,
    UsersModule,
    GamesModule,
    WordsModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
