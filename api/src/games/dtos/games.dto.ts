import { ApiHideProperty } from "@nestjs/swagger";
import { games, game_status, words, word_status } from "@prisma/client";
import { Exclude, Type } from "class-transformer";
import { Words } from "src/words/dtos/words.dto";

export class Game implements games{

    gameid: number;
    @ApiHideProperty() @Exclude()
    wordid: number;
    status: game_status;
    @Type(() => Words)
    words?: Words
    remainingTime: string

    constructor(partial: Partial<Game>){
        Object.assign(this, partial)
    }
}