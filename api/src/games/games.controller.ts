import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtSessionGuard } from 'src/session/guards/session.guard';
import { GamesService } from './games.service';

@Controller('games')
@ApiTags('Games')
export class GamesController {
  constructor(private readonly gamesService: GamesService) {}

  @Get('currentGame')
  @ApiBearerAuth()
  @UseGuards(JwtSessionGuard)
  @ApiOperation({ summary: 'Obtiene la información del juego actual y el tiempo restante para el siguiente juego. Si el usuario loggeado ya termino o perdio el juego, le muestra la palabra correcta correspondiente' })
  currentGame(@Request() req) {
    return this.gamesService.getCurrentGame(req.user);
  }

  @Post('makeAttempt/:userWord')
  @ApiBearerAuth()
  @UseGuards(JwtSessionGuard)
  @ApiOperation({ summary: 'Permite enviar a un usuario loggeado un intento al juego actual. Si el usuario adivina la palabra le gana el juego actual, pero si llega al limite de intentos pierde' })
  makeAttempt(
    @Param('userWord') userWord: string,
    @Request() req
  ) {
    return this.gamesService.makeAttempt(req.user, userWord);
  }

}
