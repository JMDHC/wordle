import { HttpException, Injectable } from '@nestjs/common';
import { Cron, CronExpression, Interval, SchedulerRegistry } from '@nestjs/schedule';
import { users_attepts } from '@prisma/client';
import { PrismaService } from 'src/prisma/prisma.service';
import { Words } from 'src/words/dtos/words.dto';
import { Game } from './dtos/games.dto';

@Injectable()
export class GamesService {

  constructor(
    private readonly prisma: PrismaService,
    private schedulerRegistry: SchedulerRegistry
){

}

  async getCurrentGame(user) {

    let cron = this.schedulerRegistry.getCronJob('games')
    let next
    try {
      next = Date.parse(cron.nextDate().toJSDate()+'');
    } catch (e) {
      next = e;
    }

    let remainingTime = this.millistoMins(next - Date.parse(new Date()+''))

    let currentGame = await this.prisma.games.findFirst({
      where: {
        status: 'Activo'
      },
      include:{
        words: true
      }
    })
    if(!currentGame){
      return {
        message: 'No hay ningun juego activo',
        remainingTime
      }
    }

    let userAttempt = await this.prisma.users_attepts.findFirst({
      where:{
        NOT:{
          status: 'Jugando'
        },
        gameid: currentGame.gameid,
        userid: user.userid
      }
    })
    
    if(!userAttempt) delete currentGame.words
    return new Game({...currentGame, remainingTime})

  }

  millistoMins(millis) {
    var minutes = Math.floor(millis / 60000);
    var seconds = +((millis % 60000) / 1000).toFixed(0);
    return (
      seconds == 60 ?
      (minutes+1) + ":00" :
      minutes + ":" + (seconds < 10 ? "0" : "") + seconds
    );
  }

  async makeAttempt(user, word: string){
    
    if(word.length != 5) throw new HttpException('La palabra ingresada debe contener 5 letras', 401)
    let _word = word.toUpperCase()

    let currentGame = await this.prisma.games.findFirst({
      where: {
        status: 'Activo'
      },
      include:{
        words: true
      }
    })
    if(!currentGame) throw new HttpException('No hay un juego activo', 402)

    let userAttempt = await this.prisma.users_attepts.findFirst({
      where:{
        gameid: currentGame.gameid,
        userid: user.userid
      }
    })

    if(!userAttempt){
      let newAttempt = await this.prisma.users_attepts.create({
        data:{
          gameid: currentGame.gameid,
          userid: user.userid
        }
      })
      return this.validateWord(currentGame.words, _word, newAttempt)
    }

    switch(userAttempt.status){
      case 'Ganado':
        throw new HttpException('Ya has ganado este juego', 402)
      case 'Perdido': 
        throw new HttpException('Ya has perdido este juego', 402)
      case 'Jugando':
        return this.validateWord(currentGame.words, _word, userAttempt)
    }

  }

  @Cron(CronExpression.EVERY_5_MINUTES, {name: 'games'})
  async createGame(){
    
    let currentGame = await this.prisma.games.findFirst({
      where: {
        status: 'Activo'
      }
    })

    if(currentGame){
      await this.prisma.games.update({
        where:{
          gameid: currentGame.gameid
        },
        data:{
          status: 'Terminado'
        }
      })
      await this.prisma.users_attepts.updateMany({
        where:{
          status: 'Jugando',
          gameid: currentGame.gameid
        },
        data:{
          status: 'Perdido'
        }
      })
      await this.prisma.words.update({
        where:{
          wordid: currentGame.wordid
        },
        data:{
          status: 'Usada'
        }
      })
    }

    let word = await this.getWord()

    if(!word){
      console.log('No quedan palabras disponibles, reiniciando tabla de palabras');
      await this.prisma.words.updateMany({
        data:{
          status: 'Activa'
        }
      })
      word = await this.getWord()
    }

    await this.prisma.games.create({
      data:{
        wordid: word.wordid
      }
    })
    
  }

  async getWord(): Promise<Words>{
    const gamesCount = await (await this.prisma.words.findMany({where:{status:'Activa'}})).length;
    const skip = Math.floor(Math.random() * gamesCount);
    let word = await this.prisma.words.findFirst({
      where:{
        status: 'Activa'
      },
      skip
    })
    return word
  }

  async validateWord(word: Words, attempt:string, user_attempt: users_attepts){
    let response = []
    let rightGuess = Array.from(word.word)
    let guess = Array.from(attempt)

    for (let i = 0; i < 5; i++) {
      let letterPosition = rightGuess.indexOf(guess[i])
      if (letterPosition === -1) {
        response.push(
          {
            "letter": guess[i],
            "value": 3
          }
        )
      } else {
          if (guess[i] === rightGuess[i]) {
            response.push(
              {
                "letter": guess[i],
                "value": 1
              }
            ) 
          } else {
            response.push(
              {
                "letter": guess[i],
                "value": 2
              }
            )
          }

      }
    }

    if (word.word === attempt) {
      await this.prisma.words.update({
        where:{
          wordid: word.wordid
        },
        data:{
          timesguessed: word.timesguessed + 1
        }
      })
      await this.prisma.users_attepts.update({
        where:{
          useratteptid: user_attempt.useratteptid
        },
        data:{
          attepts: user_attempt.attepts+1,
          status: 'Ganado'
        }
      })
      response.push(
        {
          "attepts": user_attempt.attepts+1,
          "status": 'Ganado'
        }
      )
    } else {

        if (user_attempt.attepts + 1 == 5) {
          await this.prisma.users_attepts.update({
            where:{
              useratteptid: user_attempt.useratteptid
            },
            data:{
              attepts: user_attempt.attepts+1,
              status: 'Perdido'
            }
          })
          response.push(
            {
              "attepts": user_attempt.attepts+1,
              "status": 'Perdido'
            }
          )
        }else{
          await this.prisma.users_attepts.update({
            where:{
              useratteptid: user_attempt.useratteptid
            },
            data:{
              attepts: user_attempt.attepts+1
            }
          })
          response.push(
            {
              "attepts": user_attempt.attepts+1,
              "status": 'Jugando'
            }
          )
        }

    }

    return response

  }

}
