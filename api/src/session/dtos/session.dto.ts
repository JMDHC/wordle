import { IsEmail, IsNotEmpty, IsString, MinLength } from "class-validator";

export class loginDTO{
    
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString() 
    @MinLength(8)
    password: string;

}

export class loginResponseDTO{
    token: string

    constructor(partial: Partial<loginResponseDTO>){
        Object.assign(this, partial)
    }
}