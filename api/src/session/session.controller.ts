import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { loginDTO } from './dtos/session.dto';
import { JwtSessionGuard } from './guards/session.guard';
import { SessionService } from './session.service';
import { Request } from 'express';

@Controller('session')
export class SessionController {

    constructor(
        private readonly sessionService: SessionService
    ){}

    @Post('login')
    @ApiTags('Session')
    @ApiOperation({ summary: 'Permite loggearse a un usuario para poder iniciar a jugar' })
    createUser(
        @Body() body: loginDTO
    ){
        return this.sessionService.login(body);
    }

    @Get()
    @UseGuards(JwtSessionGuard)
    @ApiOperation({ summary: 'Obtiene la información del usuario que se encuentra actualmente loggeado' })
    @ApiBearerAuth()
    @ApiTags('Session')
    getUser(
        @Req() req: Request
    ){
        return this.sessionService.getUser(req['user']);
    }

}
