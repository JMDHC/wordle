import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { PrismaModule } from 'src/prisma/prisma.module';
import { JwtStrategy } from './jwt.strategy';
import { SessionController } from './session.controller';
import { SessionService } from './session.service';

@Module({
  imports: [
    PrismaModule,
    PassportModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: {
        expiresIn: '365d'
      }
    })
  ],
  controllers: [SessionController],
  providers: [SessionService, JwtStrategy]
})
export class SessionModule {}
