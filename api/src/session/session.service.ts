import { HttpException, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { loginDTO, loginResponseDTO } from './dtos/session.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs'
import { User } from 'src/users/dtos/users.dto';

@Injectable()
export class SessionService {

    constructor(
        private readonly prisma: PrismaService,
        private readonly jwt: JwtService
    ){}

    async login(body: loginDTO){
        
        const user = await this.prisma.users.findFirst({
            where:{
                username: body.username
            }
        })

        if(!user) throw new HttpException('Usuario no encontrado', 401)
        if(! await bcrypt.compare(body.password, user.password)) throw new HttpException('La contraseña ingresada es incorrecta', 401)

        delete user.password
        const token = this.jwt.sign(user)
        return new loginResponseDTO({token})

    }

    async getUser(user_){

        const user = await this.prisma.users.findFirst({
            where:{
                username: user_.username
            },
            include:{
                users_attepts: true
            }
        })
        
        if(!user) throw new HttpException('Usuario no encontrado', 401)

        user['gamesPlayed'] = user.users_attepts.length
        user['gamesWon'] = user.users_attepts.filter(x=>x.status == 'Ganado').length
        return new User(user)

    }

    async validateUser(username: string) {
        const user = await this.prisma.users.findFirst({
            where:{
                username
            },
            select:{
                username: true,
                userid: true
            }
        });

        return user

    }

}
