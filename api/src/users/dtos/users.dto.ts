import { ApiHideProperty, ApiProperty } from '@nestjs/swagger';
import { attepts_status, users, users_attepts } from '@prisma/client';
import { Exclude, Type } from 'class-transformer';
import { IsNotEmpty, IsString, MinLength } from 'class-validator'

export class Users implements users{

    userid: number;
    username: string;
    @ApiHideProperty() @Exclude()
    password: string;

    constructor(partial: Partial<Users>){
        Object.assign(this, partial)
    }
}

export class UsersTop{

    username: string;
    gameswon: number

    constructor(partial: Partial<UsersTop>){
        Object.assign(this, partial)
    }
}

export class Attepts implements users_attepts{

    useratteptid: number;
    @ApiHideProperty() @Exclude()
    userid: number;
    @ApiHideProperty() @Exclude()
    gameid: number;
    attepts: number;
    @ApiProperty({ enum: attepts_status})
    status: attepts_status;

    constructor(partial: Partial<Attepts>){
        Object.assign(this, partial)
    }
}

export class User implements users{

    userid: number;
    username: string;
    @ApiHideProperty() @Exclude()
    password: string;
    @Type(() => Attepts)
    users_attepts?: Attepts
    gamesPlayed: number
    gamesWon: number

    constructor(partial: Partial<Users>){
        Object.assign(this, partial)
    }
}

export class userCreateDTO{
    
    @IsString()
    @IsNotEmpty()
    username: string;

    @IsString() 
    @MinLength(8)
    password: string;
}

