import { Controller, Get, Post, Body, Param, UseGuards, Request } from '@nestjs/common';
import { UsersService } from './users.service';
import { User, userCreateDTO, Users } from './dtos/users.dto'
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtSessionGuard } from 'src/session/guards/session.guard';

@Controller('users')
@ApiTags('Users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService
    ){}

    @Post()
    @ApiOperation({ summary: 'Permite dar de alta un usuario para posteriormente loggearse y poder juegar' })
    createUser(
        @Body() body: userCreateDTO
    ): Promise<User>{
        return this.usersService.createUser(body);
    }

    @Get()
    @ApiBearerAuth()
    @UseGuards(JwtSessionGuard)
    @ApiOperation({ summary: 'Obtiene el listado de todos los jugadores registrados' })
    getUsers(): Promise<Users[]>{
        return this.usersService.getUsers();
    }

    @Get(':username')
    @ApiBearerAuth()
    @UseGuards(JwtSessionGuard)
    @ApiOperation({ summary: 'Obtiene la información de un usuario especifico' })
    getUser(
        @Param('username') username: string
    ): Promise<User>{
        return this.usersService.getUser(username);
    }

    @Get('stats/topPlayers')
    @ApiBearerAuth()
    @UseGuards(JwtSessionGuard)
    @ApiOperation({ summary: 'Permite obtener a los mejores 10 jugadores ordenados por el numero de juegos ganados' })
    getTop10(){
        return this.usersService.getTop10();
    }

}
