import { Injectable, HttpException } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { User, userCreateDTO, Users } from './dtos/users.dto';
import * as bcrypt from 'bcryptjs'
import {v4 as uuidV4} from 'uuid';

@Injectable()
export class UsersService {

    constructor(
        private readonly prisma: PrismaService
    ){}

    async createUser(user: userCreateDTO): Promise<User>{


        const userExists = await this.prisma.users.findFirst({
            where: {
                username: user.username
            }
        })

        if(userExists) throw new HttpException('Ya existe una cuenta con ese nombre de usuario', 402)

        const hashedPassword = await bcrypt.hash(user.password, 10)
        
        const newUser = await this.prisma.users.create({
            data:{
                username: user.username,
                password: hashedPassword,
            }
        })

        return new User(newUser)

    }

    async getUsers(): Promise<Users[]>{
        const users = await this.prisma.users.findMany()
        return users.map(user => new Users(user));
    }

    async getUser(username: string): Promise<User>{

        const user = await this.prisma.users.findFirst(
            {
                where: {
                    username: username
                },
                include: {
                    users_attepts: true
                }
            }
        )

        if(!user) throw new HttpException('Usuario no encontrado', 401)

        user['gamesPlayed'] = user.users_attepts.length
        user['gamesWon'] = user.users_attepts.filter(x=>x.status == 'Ganado').length

        return new User(user)

    }

    async getTop10(){

        const users = await this.prisma.$queryRaw`SELECT U.username, count(*) gamesWon FROM users AS U INNER JOIN users_attepts AS UA USING (userid) WHERE UA.status = 'Ganado' GROUP BY userid ORDER BY count(*) DESC`

        return users;
    }

}
