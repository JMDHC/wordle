import { words, word_status } from "@prisma/client";

export class Words implements words{

    wordid: number;
    word: string;
    status: word_status;
    timesguessed: number;

    constructor(partial: Partial<Words>){
        Object.assign(this, partial)
    }
}