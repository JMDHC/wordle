import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { JwtSessionGuard } from 'src/session/guards/session.guard';
import { WordsService } from './words.service';

@Controller('words')
@ApiTags('Words')
export class WordsController {
  constructor(private readonly wordsService: WordsService) {}

  @Get()
    @ApiBearerAuth()
    @UseGuards(JwtSessionGuard)
    @ApiOperation({ summary: 'Permite obtener las 10 palabras mas acertadas del juego' })
    getStats(){
        return this.wordsService.getStats();
    }

}
