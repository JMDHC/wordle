import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Words } from './dtos/words.dto';

@Injectable()
export class WordsService {

    constructor(
        private readonly prisma: PrismaService
    ){}

    async getStats(){
        let words = await this.prisma.words.findMany({
            select:{
                word: true,
                timesguessed: true
            },
            orderBy:{
                timesguessed: 'desc'
            },
            take: 10
        })

        return words.map(user => new Words(user));

    }

}
