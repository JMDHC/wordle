DROP DATABASE IF EXISTS wordle;
CREATE DATABASE wordle;
USE wordle;

-- ******** WORDS ******** --
CREATE TYPE word_status AS ENUM ('Activa', 'Usada');
CREATE TABLE IF NOT EXISTS words (
    wordId				    SERIAL NOT NULL PRIMARY KEY,
    word                    VARCHAR(5) NOT NULL,
    timesGuessed            INT DEFAULT 0,
    status     		        word_status DEFAULT 'Activa'
);

-- ******** GAME ******** --
CREATE TYPE game_status AS ENUM ('Activo', 'Terminado');
CREATE TABLE IF NOT EXISTS games (
    gameId				    SERIAL NOT NULL PRIMARY KEY,
    wordId		    	    INT NOT NULL,
    status     		        game_status DEFAULT 'Activo',
    FOREIGN KEY (wordId) REFERENCES words (wordId)
);

-- ******** USERS ******** --
CREATE TABLE IF NOT EXISTS users (
    userId				    SERIAL NOT NULL PRIMARY KEY,
    username				VARCHAR(32) NOT NULL,
    password			  	VARCHAR(64) NOT NULL
);

-- ******** USER_ATTEPTS ******** --
CREATE TYPE attepts_status AS ENUM ('Jugando', 'Ganado', 'Perdido');
CREATE TABLE IF NOT EXISTS users_attepts (
    userAtteptId		    SERIAL NOT NULL PRIMARY KEY,
    userId				    INT NOT NULL,
    gameId				    INT NOT NULL,
    attepts				    INT NOT NULL DEFAULT 0,
    status     		        attepts_status DEFAULT 'Jugando',
    FOREIGN KEY (userId) REFERENCES users (userId),
    FOREIGN KEY (gameId) REFERENCES games (gameId)
);